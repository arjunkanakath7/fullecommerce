const mongoose = require('mongoose');

const productschema = new mongoose.Schema({
    name: String,
    images: [String],
    mainimage: String,
    price: Number,
    description: String   
  });

  const Product = mongoose.model('Product', productschema);

  module.exports = Product