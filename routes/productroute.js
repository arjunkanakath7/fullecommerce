const express = require('express')
const { getallproducts, getproductbyid, addproduct, updateproduct, deleteproduct } = require('../controllers/productcontroller')
const router = express.Router()

    //1. Get all products
     router.get('/', getallproducts)
  
    //2. Get a product by Id
    router.get('/:productID', getproductbyid)
  
    //3. Add new product
    router.post('/', addproduct)
  
    //4. Update product
    router.patch('/:productID', updateproduct)
    
    //5. Delete product
    router.delete('/:productID', deleteproduct)


module.exports = router