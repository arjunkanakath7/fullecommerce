const express = require('express')
const { getallcategory, getcategorybyid, addcategory, updatecategory, deletecategory } = require('../controllers/categorycontroller')
const router = express.Router()

  //1. Get all categories
  router.get('/', getallcategory)

  //2. Get a product by Id
  router.get('/:categoryID', getcategorybyid)

  //3. Add new category
  router.post('/', addcategory)
  
  //4. Update category
  router.patch('/:categoryID', updatecategory)

  //5. Delete category
  router.delete('/:categoryID', deletecategory)

module.exports = router