const Category = require("../schema/categoryschema");

 const getallcategory = async(req, res) => {
    const category = await Category.find({});
    res.json(category)
  }

 const getcategorybyid = async(req, res) => {
    try{
        const category = await Category.findById(req.params.categoryID).exec();
        res.status(200).json(category)
    }
    catch (error){
        res.status(404).send('category not found')
    }
  }

  const addcategory = async(req, res) => {
    const categorydata = req.body
    const category = new Category(categorydata);
    await category.save() 
    res.json(category)
  }

  const updatecategory = async(req, res) => {
    try{
     const updatecategory = await Category.findByIdAndUpdate(req.params.categoryID, req.body,{new:true})
      res.status(200).json(updatecategory)
    }
    catch(error){
      res.status(404).send("Category not found")
    }
  
  }

  const deletecategory = async(req, res) => {
    try{
      await Category.findByIdAndDelete(req.params.categoryID)
      res.status(200).send('Deleted')
    }
    catch(error){
      res.status(404).send("category not found")
    }
  }

  module.exports = {
    getallcategory,
    getcategorybyid,
    addcategory,
    updatecategory,
    deletecategory
  }