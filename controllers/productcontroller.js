const Product = require("../schema/productschema");

const getallproducts = async(req, res) => {
    const product = await Product.find({});
    res.json(product)
  }

const getproductbyid = async(req, res) => {
    try{
        const product = await Product.findById(req.params.productID).exec();
        res.status(200).json(product)
    }
   catch(error){
    res.status(404).send('product not found')
   }
  }  

const addproduct = async(req, res) => {
    const productdata = req.body
    const product = new Product(productdata);
    await product.save();
    res.status(201).json(product)
  }

const updateproduct = async(req, res) => {
  try{
    const updateproduct = await Product.findByIdAndUpdate(req.params.productID, req.body, {new: true})
    res.status(200).json(updateproduct)
  }
  catch(error){
    res.status(404).send('product category not found')
  }
  
  }

const deleteproduct = async(req, res) => {
  try{
    await Product.findByIdAndDelete(req.params.productID)
    res.status(200).send('Deleted')
  }
  catch(error){
    res.status(404).send("product Category not found")
  }
  }  

  module.exports = {
    getallproducts,
    getproductbyid,
    addproduct,
    updateproduct,
    deleteproduct
  }